# syt5-gk923-shared-storage

## Kacper Urbaniec | 5AHIT | 05.12.2019

# iSCSI Setup

## Voraussetzungen

- 2 virtuelle Maschinen (iSCSI Target und iSCSI Initiator) mit installiertem Linux (GUI nicht erforderlich), empfohlenes Betriebssystem: Debian/Ubuntu/CentOS.
  - eine (iSCSI Target) davon mit einer zweiten (virtuellen) Festplatte (diese soll über iSCSI freigegeben werden)
- Kenntniss der iSCSI Terminologie (IQN, Target, Initiator, LUN, ...)

## Aufgabenstellung

### iSCSI Target

- Einrichten des iSCSI Storage Devices (die zweite Festplatte gehört partitioniert)
- Installation des iSCSI Targets (CentOS: `yum install targetcli`)
- Erstellen des Storage-Backends (in `targetcli`: `create dev=/dev/sd... name=sd...` - statt `...` den Namen der erstellten Partition einsetzen)
- Erstellen des iSCSI Targets (in `targetcli`: `create wwn=iqn.2019-12.at.ac.tgm:syt`)
- (eventuell: erstellen einer ACL)
- LUNs zum iSCSI Target hinzufügen (in `targetcli`: `create /backstores/block/sd...` im *Verzeichnis* `iscsi/iqn.2019-12.at.ac.tgm:syt/tpg1/luns`)

### iSCSI Initiator

- Installation des Initiators (CentOS: `yum install iscsi-initiator-utils`, Ubuntu: `apt-get install open-iscsi`)
- Erstellen des Initiator-Namens (CentOS: Datei `/etc/iscsi/initiatorname.iscsi`, empfohlener Wert: `InitiatorName=iqn.2019-12.at.ac.tgm:node1`, danach `iscsid` neu starten)
- iSCSI Target finden (`iscsiadm --mode discovery --type sendtargets --portal  --discover`)
- Mit Target verbinden (`iscsiadm --mode node --targetname inn.2019-12.at.ac.tgm:syt --login`) - ein neues Gerät sollte nun im Kernel-Log ersichtlich sein, bzw. mittels `lsscsi` aufgelistet werden.

### erweiterte Aufgabenstellung zu iSCSI

- ACL und Authentifizierung konfigurieren.
- Alternative iSCSI-Targets testen (auch z.B.: Windows Server, FreeNAS, ...)

## hilfreiche Links

- [How to configure iSCSI target and initiator on CentOS/RHEL 7/8 Linux](https://www.golinuxcloud.com/configure-iscsi-target-initiator-targetcli-rhel-centos-7/)
- [How to install and Configure iSCSI Storage Server on Ubuntu 18.04](https://kifarunix.com/how-to-install-and-configure-iscsi-storage-server-on-ubuntu-18-04/)
- [Ubuntu 18.04 LTS: Configure iSCSI Target (tgt)](https://www.server-world.info/en/note?os=Ubuntu_18.04&p=iscsi&f=2)
- Alternative Lösungen für das iSCSI Target: `targetcli`, `targetcli-fb`, `tgt`, `istgt`, `iscsitarget`

# Cluster Dateisystem(e)

## Voraussetzungen

- 2 virtuelle Maschinen (node1,node2) für das Cluster-Dateisystem mit verbundenen gemeinsamen Storage, z.B.:
  - die iSCSI Target Maschine aus der obigen Aufgabenstellung
  - [How To Share Disks in VirtualBox Between Linux Guest OS](https://www.unixmen.com/share-disks-virtualbox-linux-guest-os/)
  - [Building a VMware Shared Disk](https://www.symantec.com/connect/articles/building-vmware-shared-disk)

## Aufgabenstellung

### Oracle Cluster Filesystem 2

- OCFS2 Tools installieren (`ocfs2-tools`)
- Cluster-Configuration (/etc/ocfs2/cluster.conf) einrichten (Vorlage z.B. unter /usr/share/doc/ocfs2-tools/examples)
  - Hostnamen und IP-Adressen müssen stimmen! Beide Nodes müssen unterschiedliche Hostnamen haben!
- OCFS2 Konfiguration aktualisieren (`/etc/init.d/o2cb configure` oder `dpkg-reconfigure ocfs2-tools`)
- Shared Disk mit OCFS2 formatieren (`mkfs.ocfs2`)
- Shared Disk auf beiden Nodes mounten.
- Testen, wie sich das System bei gleichzeitigem Zugriff verhält.

### erweiterte Aufgabenstellung zu Cluster Filesystems

- OCFS2 mit Pacemaker Integration.
- Konfiguration von GFS2 statt OCFS2.

## hilfreiche Links

- [Installing and Configuring an OCFS2 Clustered File System](https://www.shapeblue.com/installing-and-configuring-an-ocfs2-clustered-file-system/)
- [Ubuntu Manpage: OCFS2 - A Shared-Disk Cluster File System for Linux](http://manpages.ubuntu.com/manpages/bionic/man7/ocfs2.7.html)
- [How to set up GFS2 with clustering on Linux](https://www.golinuxcloud.com/configure-gfs2-setup-cluster-linux-rhel-centos-7/) (erweitert)

# Abgabemodalitäten

- Vorzeigen der abgeschlossenen Konfiguration von (sofort, nicht erst wenn alles fertig ist!):
  - iSCSI Target
  - iSCSI Initiator
  - OCFS2
- Dokumentation: verwendete Befehle (+Beschreibung was diese konkret machen), erwartete und reale Ausgaben, aufgetretene Probleme, Fehlersuche und Problembehebung.

# Implementierung

## Aufgabenstellung

### iSCSI Target (Node2)

- Einrichten des iSCSI Storage Devices (die zweite Festplatte gehört partitioniert)

  Dem iSCSI Target muss eine zweite virtuelle Festplatte hinzugefügt werden. Dazu muss in VMware bei der virtuellen Maschine in die `Virtual Machine Setttings` gehen, dann unten `Add` drücken und eine neue Festplatte erstellen.

  Wenn man jetzt das System startet, sollte die neue Festplatte mittels `fdisk -l` angezeigt werden. Meine neue virtuelle Festplatte mit 2 Gigabyte war unter `/dev/sdb` zu finden.

  

- Installation des iSCSI Targets 

  ```
  sudo apt -y install targetcli-fb
  ```

  

- Erstellen des Storage-Backends

  ```
  sudo targetcli
  ```

  ```
  backstores/block create name=sdb dev=/dev/sdb
  ```

  

- Erstellen des iSCSI Targets 

  ```
  sudo targetcli
  ```

  ```
  iscsi/
  create wwn=iqn.2019-12.at.ac.tgm:syt
  ```

  

- Erstellen einer ACL

  Eine Access Control List wird gebraucht um den Clients, den iSCSI Initators, zu erlauben, auf das Target zugreifen zu können. Dabei wird der IQN (iSCSI Qualified Name) des zukünftigen Client angegeben.

  ```
  sudo targetcli
  ```

  ```
  iscsi/iqn.2019-12.at.ac.tgm:syt/tpg1/acls
  create iqn.2019-12.at.ac.tgm:node1
  ```

  ![](images/1_1.PNG)

  

- LUNs zum iSCSI Target hinzufügen

  ```
  sudo targetcli
  ```

  ```
  iscsi/iqn.2019-12.at.ac.tgm:syt/tpg1/luns
  create /backstores/block/sdb
  ```

  ![](images/1.PNG)

### iSCSI Initiator (Node1)

- Installation des Initiators 

  ```
  sudo apt -y install open-iscsi
  ```

  

- Erstellen des Initiator-Namens

  ```
  sudo vim /etc/iscsi/initiatorname.iscsi
  ```

  Im File muss als Initiator die IQN des Clients angegeben werden, dem wir beim Target in der ACL festgelegt haben. 

  ```
  InitiatorName=iqn.2019-12.at.ac.tgm:node1
  ```

  

- iSCSI Target finden 

  Als nächstes muss der Initiator den Target aufsuchen. Dazu wird der nächste Befehl verwenden, wobei  bei `-p` (Portal) die IP-Adresse des Targets angegeben wird.

  ```
  sudo iscsiadm -m discovery -t sendtargets -p 192.168.84.129
  ```

  ![](images/2.PNG)

  

- Mit Target verbinden 

  Falls das Target gefunden wurde, kann man sich mithilfe seines Namens verbinden.
  
  ```
  sudo iscsiadm --mode node --targetname iqn.2019-12.at.ac.tgm:syt --login
  ```

  ![](images/3.PNG)

  

  Wenn das Anmelden erfolgreich war, kann man mit dem nachfolgenden Befehl beim Client testen, ob  der Target korrekt eingebunden wurde (bei mir `sdb`).
  
  ```
  lsscsi
  ```
  
  ![](images/4.PNG)

## Aufgabenstellung

### Oracle Cluster Filesystem 2

* Shared Disk unter VMware einrichten

  Zuerst muss eine virtuelle Festplatte erstellt werden, die von den zwei VMs genutzt werden soll. Zuerst erstellt man eine neue Festplatte unter  `Virtual Machine Setttings` , dann unten `Add` drücken und eine neue Festplatte erstellen. Bei der der zweiten VM wird dann diese wieder verwendet, d.h. beim `Add` Vorgang wählt man aus, dass man eine bestehende virtuelle Festplatte verwenden möchte.

  Der nächste Schritt ist es, die `vmx` Datei der beiden VMs zu bearbeiten. Dabei muss die neue virtuelle Festplatte bearbeitet werden, ihre Konfigurationen sind meist am Ende der Datei zu finden.  Die Datei sollte ungefähr so aussehen, dabei ist `scsi0:2` die neue shared disk.

  ```
  scsi1.virtualDev = "lsilogic"
  scsi1.present = "TRUE"
  scsi0:2.fileName = "C:\Users\aon91\Documents\VMware\HardDisk\shareddisk.vmdk"
  scsi0:2.present = "TRUE"
  ```

  Folgende Änderungen müssen durchgenommen werden:

  ```
  diskLib.dataCacheMaxSize = "0"
  scsi1.virtualDev = "lsilogic"
  scsi1.present = "TRUE"
  scsi0:2.fileName = "C:\Users\aon91\Documents\VMware\HardDisk\shareddisk.vmdk"
  scsi0:2.present = "TRUE"
  scsi0:2.mode = "independent-persistent"
  scsi0:2.shared = "TRUE"
  scsi0:2.redo = ""
  ```

- OCFS2 Tools installieren 

  Auf beiden Nodes installieren:

  ```
  sudo apt -y install ocfs2-tools
  ```

  

- Cluster-Configuration 

  Als nächstes muss auf beiden Nodes die Cluster Konfiguration für o2cb (Cluster Stack für unser späteres geteiltes OCFS2 File-System) erstellt werden. Zuerst muss die Datei angelegt werden.

  ```
  sudo touch /etc/ocfs2/cluster.conf
  sudo chmod 777 /etc/ocfs2/cluster.conf
  ```
  
  Als nächstes wird die Konfiguration eingetragen. Dabei muss beachtet werden, dass  Subkonfigurationen mittels Tabulator eingerückt werden müssen, ansonsten kann die Konfiguration nicht gelesen werden. In die Konfiguration selbst gibt man alle Nodes an, ihre Namen und IP-Adressen. Jede Node muss auch einem Cluster hinzugefügt werden, der am Schluss konfiguriert wird.

  ```
  sudo vim /etc/ocfs2/cluster.conf
  ```

  ```
  node:
    ip_port = 7777
  	ip_address = 192.168.84.130
  	number = 0
  	name = node1
  	cluster = ocfs2
   
  node:
  	ip_port = 7777
  	ip_address = 192.168.84.129
  	number = 1
  	name = node2
  	cluster = ocfs2
   
  cluster:
  	node_count = 2
  	name = ocfs2
  ```
  
  

- OCFS2 Konfiguration aktualisieren 

  Jetzt muss auch beiden Nodes die Konfiguration neu eingelesen werden.

  ```
  sudo dpkg-reconfigure ocfs2-tools
  ```

  Einstellungen:

  * Run on startup - Yes
  * Cluster name - ocfs2
  * Heartbeat - 31
  * Timeout - 30000
  * Keep Alive - 2000
  * Reconnect Delay - 2000

  

- Shared Disk mit OCFS2 formatieren (`mkfs.ocfs2`)

  Als nächstes wird die virtuelle Festplatte als OCFS2 File System formatiert. Dies muss genau auf einer Node gemacht werden. Ich habe dies auf meiner zweiten Node gemacht, die die Festplatte unter `/dev/sdc` eingebunden hat. Hätte ich meine erste Node dazu verwendet, müsste ich `/dev/sdb` verwenden.

  ```
  sudo mkfs.ocfs2 -L ocfs2 -T vmstore --fs-feature-level=max-compat /dev/sdc -N 3 --force
  ```
  
  

- Shared Disk auf beiden Nodes mounten.

  Als nächstes kann die die shared Disk auf beiden Nodes gemounted werden. Der Vorgang ist auf beiden Nodes ähnlich.

  

  **Node2:**

  ```
  sudo mkdir /shared-disk
  ```

  ```
  sudo vim /etc/fstab
  ```

  Folgende Zeile hinzufügen:

  ```
  /dev/sdc /shared-disk ocfs2 _netdev,nointr 0 0
  ```

  Cluster neu starten und automatischen Start aktivieren

  ```
  sudo service o2cb restart
  sudo systemctl enable o2cb
  sudo systemctl enable ocfs2
  ```

  Shared Disk mounten

  ```
  sudo mount -a
  ```

  Falls eine Fehlermeldung wie nachfolgend beim mount kommt, ist wahrscheinlich die Cluster-Konfiguration nicht richtig geladen worden, was durch Syntax-Fehler entstehen kann.

  ![](images/6.PNG)

  

  **Node1:**

  ```
  sudo mkdir /shared-disk
  ```

  ```
  sudo vim /etc/fstab
  ```

  Folgende Zeile hinzufügen:

  ```
  /dev/sdc /shared-disk ocfs2 _netdev,nointr 0 0
  ```

  Cluster neu starten und automatischen Start aktivieren

  ```
  sudo service o2cb restart
  sudo systemctl enable o2cb
  sudo systemctl enable ocfs2
  ```

  Shared Disk mounten

  ```
  sudo mount -a
  ```

  

- Testen, wie sich das System bei gleichzeitigem Zugriff verhält.

  Wenn die Konfiguration erfolgreich war, sollte im Kernel-Log (mittels `dmesg` einsehbar) folgende Meldung zu finden sein:

  ![](images/working.PNG)

  

  Danach sollte die Shared Disk funktionieren, wenn ich jetzt eine Datei auf Node 2 anlege, sollte diese auf Node 1 sichtbar sein und vice versa.

  ![](images/8.PNG)
  
  
  
  ![](images/9.PNG)

  
  
  Falls dies nicht funktioniert, kann es vielleicht an den IP-Tables oder der Firewall des Gerätes liegen. Zum testen kann man einfach alles erlauben.
  
  ```
  sudo iptables -P INPUT ACCEPT
  sudo iptables -P OUTPUT ACCEPT
  sudo iptables -P FORWARD ACCEPT
  sudo iptables -F
  sudo ufw disable
  ```
  
  Weiters kann man testen ob ein korruptes File-System vorliegt.
  
  ```
  fsck.ocfs2 [disk name]
  ```
  
  Was bei mir nach einem Neustart geholfen hat, war ein erneutes mounten.
  
  ```
  # Node2
  sudo umount /dev/sdc
  sudo mount -t ocfs2 /dev/sdc /shared-disk/
  # Node1
  sudo umount /dev/sdb
  sudo mount -t ocfs2 /dev/sdb /shared-disk/
  ```
  
  

## Quellen

* [OCFS2 Cluster | 05.12.2019](https://www.youtube.com/watch?v=Mw5LzHVX7AU)
* [O2CB Man Page | 05.12.2019](https://www.mankier.com/8/o2cb)
